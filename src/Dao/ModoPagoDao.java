package Dao;

import Conexion.ConexionDB;
import Modelo.ModoPagoBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ModoPagoDao {

    ConexionDB conn;

    public ModoPagoDao(ConexionDB conn) {
        this.conn = conn;
    }

    public boolean insertarMPago(ModoPagoBean mpbean) {
        String sql = "insert into modo_pago values(?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, mpbean.getNum_pago());
            ps.setString(2, mpbean.getNombre());
            ps.setString(3, mpbean.getOtros_detalles());
            ps.executeUpdate();

            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    public boolean actualizarMPago(ModoPagoBean mpbean) {
        String sql = "update modo_pago set id_cliente=?, fecha =?, num_pago=?, where num_factura=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, mpbean.getNombre());
            ps.setString(2, mpbean.getOtros_detalles());
            ps.setInt(3, mpbean.getNum_pago());
            ps.executeUpdate();

            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    public List<ModoPagoBean> consultarAll() {
        String sql = "select * from modo_pago";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<ModoPagoBean> listaPago = new LinkedList<>();
            ModoPagoBean mpb;
            while (rs.next()) {
                mpb = new ModoPagoBean(rs.getInt("num_pago"));
                mpb.setNombre(rs.getString("nombre"));
                mpb.setOtros_detalles(rs.getString("otros_detalles"));

                listaPago.add(mpb);
            }
            return listaPago;
        } catch (SQLException e) {
        }
        return null;
    }

    public boolean eliminar(int modo_pago) {
        String sql = "delete from modo_pago where num_pago=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, modo_pago);
            ps.executeQuery();
            return true;
        } catch (SQLException e) {
            return false;
        }

    }

}
