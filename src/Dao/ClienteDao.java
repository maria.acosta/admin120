package Dao;

import Conexion.ConexionDB;
import Modelo.ClienteBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ClienteDao {

    ConexionDB conn;

    public ClienteDao(ConexionDB conn) {
        this.conn = conn;
    }

    public boolean insertarCliente(ClienteBean clienteb) {
        String sql = "insert into cliente values(?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, clienteb.getId_cliente());
            ps.setString(2, clienteb.getNombre());
            ps.setString(3, clienteb.getApellido());
            ps.setString(4, clienteb.getDireccion());
            ps.setString(5, clienteb.getFecha_nacimiento());
            ps.setString(6, clienteb.getTelefono());
            ps.setString(7, clienteb.getEmail());
            ps.executeUpdate();

            System.out.println("INSERTAR CLIENTE CORRECTO/CLIENTEDAO/");
            return true;
        } catch (SQLException e) {
            System.out.println("ERROR AL INSERTAR CLIENTE/CLIENTEDAO/" + e);
            return false;
        }

    }

    public boolean actualizarCliente(ClienteBean clienteb) {
        String sql = "update cliente set nombre=?, apellido=?, direccion=?, fecha_nacimiento=?, telefono=?, email=? where id_cliente=?;";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, clienteb.getNombre());
            ps.setString(2, clienteb.getApellido());
            ps.setString(3, clienteb.getDireccion());
            ps.setString(4, clienteb.getFecha_nacimiento());
            ps.setString(5, clienteb.getTelefono());
            ps.setString(6, clienteb.getEmail());
            ps.setInt(7, clienteb.getId_cliente());
            ps.executeUpdate();

            System.out.println("ACTUALIZAR CORRECTO CLIENTES/CLIENTEDAO/");
            return true;
        } catch (SQLException e) {
            System.out.println("ERROR AL ACTUALIZAR CLIENTES/CLIENTEDAO/" + e);
            return false;
        }

    }

    public List<ClienteBean> consultarAll() {
        String sql = "select * from cliente";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<ClienteBean> listaCliente = new LinkedList<>();
            ClienteBean clientb;
            while (rs.next()) {
                clientb = new ClienteBean(rs.getInt("id_cliente"));
                clientb.setNombre(rs.getString("nombre"));
                clientb.setApellido(rs.getString("apellido"));
                clientb.setDireccion(rs.getString("direccion"));
                clientb.setFecha_nacimiento(rs.getString("fecha_nacimiento"));
                clientb.setTelefono(rs.getString("telefono"));
                clientb.setEmail(rs.getString("email"));
                listaCliente.add(clientb);

            }
                System.out.println("CONSULTAR TODO  CLIENTES CORRECTO/CLIENTEDAO/ "+listaCliente);
            return listaCliente;
        } catch (SQLException e) {
            System.out.println("ERROR AL CONSULTAR TODOS LOS  CLIENTES/CLIENTEDAO/" + e);
            return null;
        }

    }

    public List<ClienteBean> consultarById(int id_cliente) {
        String sql = "select from cliente where id_cliente=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id_cliente);
            ResultSet rs = ps.executeQuery();
            List<ClienteBean> listaCliente = new LinkedList<>();
            ClienteBean clientb;
            while (rs.next()) {
                clientb = new ClienteBean(rs.getInt("id_cliente"));
                clientb.setNombre(rs.getString("nombre"));
                clientb.setApellido(rs.getString("apellido"));
                clientb.setDireccion(rs.getString("direccion"));
                clientb.setFecha_nacimiento(rs.getString("fecha_nacimiento"));
                clientb.setTelefono(rs.getString("telefono"));
                clientb.setEmail(rs.getString("email"));
                listaCliente.add(clientb);

            }
            return listaCliente;
        } catch (SQLException e) {
            System.out.println("ERROR AL CONSULTAR EL  CLIENTE/CLIENTEDAO/" + e);
            return null;
        }

    }

    /*    public boolean eliminar(int id_cliente) {
        String sql = "delete from cliente where id_cliente=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id_cliente);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }*/
    public static void main(String[] args) {

        int id_cliente = 2;
        String nombre = "Fabricio";
        String apellido = "Rodriguez";
        String direccion = "Colonica Las Brisas";
        String fecha_nacimiento = "2000-02-01";
        String telefono = "2266-5010";
        String email = "fabricio.rodriguez.com";
        /*-------------------------------------------*/
        ConexionDB conn = new ConexionDB();
        ClienteBean cb = new ClienteBean(0);
        ClienteDao cd = new ClienteDao(conn);
        /*--------------------------------------------*/
        cb.setId_cliente(id_cliente);
        cb.setNombre(nombre);
        cb.setApellido(apellido);
        cb.setDireccion(direccion);
        cb.setFecha_nacimiento(fecha_nacimiento);
        cb.setTelefono(telefono);
        cb.setEmail(email);
        /*--------------------------------------------*/

//        cd.insertarCliente(cb);
//        cd.actualizarCliente(cb);
      

    }
}
