package Dao;

import Conexion.ConexionDB;
import Modelo.CategoriaBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class CategoriaDao {

    ConexionDB conn;

    public CategoriaDao(ConexionDB conn) {
        this.conn = conn;
    }

    public boolean createCategoria(CategoriaBean cat) {
        String sql = "insert into categoria values(?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, cat.getId_categoria());
            ps.setString(2, cat.getNombre());
            ps.setString(3, cat.getDescripcion());
            ps.executeUpdate();

            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    public boolean updateCategoria(CategoriaBean cat) {
        String sql = "update categoria set nombre=?, otros_detalles=?, where num_pago=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, cat.getNombre());
            ps.setString(2, cat.getDescripcion());
            ps.setInt(3, cat.getId_categoria());
            ps.executeUpdate();

            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    public List<CategoriaBean> readCategoria() {
        String sql = "select * from categoria";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<CategoriaBean> read = new LinkedList<>();
            CategoriaBean cat;
            while (rs.next()) {
                cat = new CategoriaBean(rs.getInt("id_categoria"));
                cat.setNombre(rs.getString("nombre"));
                cat.setDescripcion(rs.getString("descripcion"));
                read.add(cat);

            }
            return read;
        } catch (Exception e) {
            return null;
        }

    }

    public List<CategoriaBean> readAllCategoria(int id_categoria) {
        String sql = "select from categoria where id_categoria=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id_categoria);
            ResultSet rs = ps.executeQuery();
            List<CategoriaBean> read = new LinkedList<>();
            CategoriaBean cat;
            while (rs.next()) {
                cat = new CategoriaBean(rs.getInt("id_categoria"));
                cat.setNombre(rs.getString("nombre"));
                cat.setDescripcion(rs.getString("descripcion"));
                read.add(cat);

            }
            return read;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean eliminar(int id_categoria) {
        String sql = "delete from categoria where id_categoria=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id_categoria);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}
