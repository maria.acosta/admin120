package Dao;

import Conexion.ConexionDB;
import Modelo.CategoriaBean;
import Modelo.ProductoBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

public class ProductoDao {

    ConexionDB conn;

    public ProductoDao(ConexionDB conn) {
        this.conn = conn;
    }

    public boolean create(ProductoBean product, CategoriaBean cat) {
        String sql = "insert into producto values (?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, product.getId_producto());
            ps.setString(2, product.getNombre());
            ps.setDouble(3, product.getPrecio());
            ps.setString(4, product.getStock());
            ps.setInt(5, cat.getId_categoria());
            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public boolean edit(ProductoBean product, CategoriaBean cat) {
        String sql = "update producto set nombre=?, precio=?, stock=?, id_categoria=?, where id_producto=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, product.getNombre());
            ps.setDouble(2, product.getPrecio());
            ps.setString(3, product.getStock());
            ps.setInt(4, cat.getId_categoria());
            ps.setInt(5, product.getId_producto());
            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public List<ProductoBean> read() {
        String sql = "seleect * from producto";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<ProductoBean> read_product = new LinkedList<>();
            ProductoBean pb;
            CategoriaBean cat;
            while (rs.next()) {
                pb = new ProductoBean(rs.getInt("id_producto"));
                cat = new CategoriaBean(rs.getInt("id_categoria"));
                pb.setId_producto(rs.getInt("id_producto"));
                pb.setNombre(rs.getString("nombre"));
                pb.setPrecio(rs.getDouble("precio"));
                pb.setStock(rs.getString("stock"));

            }
            return read_product;
        } catch (Exception e) {
            return null;
        }
    }

    public List<ProductoBean> readAll(int id_producto) {
        String sql = "seleect * from producto where id_producto=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id_producto);
            ResultSet rs = ps.executeQuery();
            List<ProductoBean> read_product = new LinkedList<>();
            ProductoBean pb;
            CategoriaBean cat;
            while (rs.next()) {
                pb = new ProductoBean(rs.getInt("id_producto"));
                cat = new CategoriaBean(rs.getInt("id_categoria"));
                pb.setId_producto(rs.getInt("id_producto"));
                pb.setNombre(rs.getString("nombre"));
                pb.setPrecio(rs.getDouble("precio"));
                pb.setStock(rs.getString("stock"));

            }
            return read_product;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(int id_producto) {
        String sql = "delete from producto where id_producto=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id_producto);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

}
