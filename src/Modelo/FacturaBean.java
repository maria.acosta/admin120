package Modelo;

public class FacturaBean {

    private int num_factura;
    private ClienteBean id_cliente;
    private String fecha;
    private ModoPagoBean num_pago;

    public int getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(int num_factura) {
        this.num_factura = num_factura;
    }

    public ClienteBean getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(ClienteBean id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public ModoPagoBean getNum_pago() {
        return num_pago;
    }

    public void setNum_pago(ModoPagoBean num_pago) {
        this.num_pago = num_pago;
    }

   
    

}
