package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionDB {

    static String bd  = "participante23";
    static String user = "root";
    static String pass = "root";
      static String url ="jdbc:mysql://localhost/"+bd+"?useSSL=false";
    Connection conn = null;

    public ConexionDB (){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url,user, pass);
            if(conn!=null){
                System.out.println("CONEXION ESTABLECIDA");
            } 
            
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("ERROR EN LA CONEXION "+e);
        }
    
    }
    
    public Connection conectar() {
        return conn;
    }

    public void desconectar() throws Exception {
        conn.close();
    }

    public static void main(String[] args) {
        ConexionDB conn = new ConexionDB();
    }

}
